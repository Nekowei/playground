﻿using UnityEngine;

namespace Nekowei.Catlike
{
    [System.Serializable]
    public struct HexCoordinates
    {
		[SerializeField]
		private int x, z;
		public int X { get { return x; } }
        public int Z { get { return z; } }
		public int Y { get { return -X - Z; } }

		public HexCoordinates(int x, int z)
		{
			this.x = x;
			this.z = z;
		}

		public static HexCoordinates FromOffsetCoordinates(int x, int z)
		{
			return new HexCoordinates(x - z / 2, z);
		}

		public override string ToString()
		{
			return "(" + X.ToString() + ", " + Y.ToString() + ", " + Z.ToString() + ")";
		}

		public string ToStringOnSeparateLines()
		{
			return X.ToString() + "\n" + Y.ToString() + "\n" + Z.ToString();
		}

		public static HexCoordinates FromPosition(Vector3 position)
		{
			float x = position.x / (HexMetrics.innerRadius * 2f);
			float y = -x;
			float offset = position.z / (HexMetrics.outerRadius * 3f);
			x -= offset;
			y -= offset;
			int iX = Mathf.RoundToInt(x);
			int iY = Mathf.RoundToInt(y);
			int iZ = Mathf.RoundToInt(-x - y);
			// 处理边缘情况
			if (iX + iY + iZ != 0)
			{
				float dX = Mathf.Abs(x - iX);
				float dY = Mathf.Abs(y - iY);
				float dZ = Mathf.Abs(-x - y - iZ);

				if (dX > dY && dX > dZ)
				{
					iX = -iY - iZ;
				}
				else if (dZ > dY)
				{
					iZ = -iX - iY;
				}
			}
			return new HexCoordinates(iX, iZ);
		}

		public Vector3Int ToChunkCoordinates()
        {
			return new Vector3Int(X / HexMetrics.chunkSizeX, 0, Z / HexMetrics.chunkSizeZ);
        }

		public HexCoordinates GetNeighbor(HexDirection dir)
        {
            return dir switch
            {
                HexDirection.NE => new HexCoordinates(X, Z + 1),
                HexDirection.E => new HexCoordinates(X + 1, Z),
                HexDirection.SE => new HexCoordinates(X + 1, Z - 1),
                HexDirection.SW => new HexCoordinates(X, Z - 1),
                HexDirection.W => new HexCoordinates(X - 1, Z),
                HexDirection.NW => new HexCoordinates(X - 1, Z + 1),
                _ => this,
            };
        }

		public int Distance(HexCoordinates newHc)
        {
			int dx = Mathf.Abs(X - newHc.X);
			int dy = Mathf.Abs(Y - newHc.Y);
			int dz = Mathf.Abs(Z - newHc.Z);
			return Mathf.Max(dx, dy, dz);
        }

	}
}
