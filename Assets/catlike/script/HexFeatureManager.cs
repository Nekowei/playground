using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Nekowei.Catlike
{
	public class HexFeatureManager : MonoBehaviour
	{

		public Transform treePrefab;

		Transform container;

		public void Clear() {
			if (container)
			{
				Destroy(container.gameObject);
			}
			container = new GameObject("Features Container").transform;
			container.SetParent(transform, false);
		}

		public void Apply() { }

		public void AddFeature(Vector3 position)
		{
			HexHash hash = HexMetrics.SampleHashGrid(position);
			if (hash.a >= 0.5f) 
			{
				return; // use the first one to decide whether we actually add a feature, or skip it
			}
			Transform instance = Instantiate(treePrefab);
			instance.localPosition = HexMetrics.Perturb(position);
			instance.localRotation = Quaternion.Euler(0f, 360f * hash.b, 0f);
			instance.SetParent(container, false);
		}

	}
}

