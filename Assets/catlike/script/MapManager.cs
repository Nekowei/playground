﻿using Nekowei.PlayGround;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Nekowei.Catlike
{
    public class MapManager : MonoBehaviour
    {
		public int dataVersion;
        public Color[] colors;
		public GameObject hexGridPrefab;
        public static HexGrid hexGrid;

		public GameObject hexMapCameraPrefab;
		private GameObject hexMapCamera;

		public int seed;
		public Texture2D noiseSource;

		public int maxElevation;
		public static bool showLabel;
		public static int worldSize;
		public static int seaLevel;

		public BoolEventSO showLabelToggleEvent;
		public FloatEventSO worldSizeChangeEvent;
		public FloatEventSO seaLevelChangeEvent;

		void Awake()
        {
			HexMetrics.noiseSource = noiseSource;
			HexMetrics.InitializeHashGrid(seed);
			HexMetrics.colors = colors;
		}

		// This method will get invoked after a recompile
		void OnEnable()
		{
			if (!HexMetrics.noiseSource)
			{
				HexMetrics.noiseSource = noiseSource;
				HexMetrics.InitializeHashGrid(seed);
				HexMetrics.colors = colors;
			}
			showLabelToggleEvent.action += ShowLabel;
			worldSizeChangeEvent.action += OnWorldSizeChange;
			seaLevelChangeEvent.action += SetSeaLevel;
		}

		void OnDisable()
		{
			showLabelToggleEvent.action -= ShowLabel;
			worldSizeChangeEvent.action -= OnWorldSizeChange;
			seaLevelChangeEvent.action -= SetSeaLevel;
		}

		void Start()
		{
			worldSize = 1;
			seaLevel = 1;
			showLabel = false;

			hexGrid = Instantiate(hexGridPrefab).GetComponent<HexGrid>();

			hexMapCamera = Instantiate(hexMapCameraPrefab);
			hexMapCamera.GetComponent<HexMapCamera>().hexGrid = hexGrid;
		}

		public void RefreshWorld()
		{
			HexMetrics.InitializeHashGrid(seed);
			hexGrid.RollElevation();
		}

		public void ShowLabel(bool value)
        {
			showLabel = value;
			hexGrid.RefreshAll();
		}

		public void AddRiver()
        {
			hexGrid.CreateRiver();
        }

		public void OnWorldSizeChange(float value)
        {
			worldSize = (int)value;
		}

		public void SetSeaLevel(float value)
		{
			seaLevel = (int)value;
		}

		public void Save()
		{
			string path = Path.Combine(Application.persistentDataPath, "test.map");
			Debug.Log("saving " + path);
            using BinaryWriter writer = new BinaryWriter(File.Open(path, FileMode.Create));
			writer.Write(dataVersion);
			hexGrid.Save(writer);
        }

		public void Load()
		{
			string path = Path.Combine(Application.persistentDataPath, "test.map");
			Debug.Log("loading " + path);
			using BinaryReader reader = new BinaryReader(File.OpenRead(path));
			int v = reader.ReadInt32();
			if (v.Equals(dataVersion))
			{
				hexGrid.Load(reader);
            }
			else
			{
				Debug.LogError("load fail: wrong version!");
			}
        }

	}
}

