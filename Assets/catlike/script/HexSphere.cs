﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Nekowei.Catlike
{
    [RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
    public class HexSphere : MonoBehaviour
    {

        public int size;

        private Mesh mesh;

        public void Start()
        {
            GetComponent<MeshFilter>().mesh = mesh = new Mesh();
            mesh.name = "Sphere Grid";

            Vector3[] vertices = new Vector3[6];
            vertices[0] = new Vector3(0, 0, 0);
            vertices[1] = new Vector3(1, 0, 0);
            vertices[2] = new Vector3(1, 1, 0);
            vertices[3] = new Vector3(2, 0, 0);
            vertices[4] = new Vector3(2, 1, 0);
            vertices[5] = new Vector3(3, 2, 0);
            mesh.vertices = vertices;

            int[] triangles = new int[vertices.Length];
            triangles[0] = 0;
            triangles[1] = 1;
            triangles[2] = 2;
            triangles[3] = 3;
            triangles[4] = 4;
            triangles[5] = 5;
            mesh.triangles = triangles;

        }
    }
}
