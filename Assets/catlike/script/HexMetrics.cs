﻿using UnityEngine;

namespace Nekowei.Catlike
{
    public static class HexMetrics
    {

		public const int chunkSizeX = 5, chunkSizeZ = 5;

		public const float outerToInner = 0.866025404f;
		public const float innerToOuter = 1f / outerToInner;
		public const float outerRadius = 10f;
		public const float innerRadius = outerRadius * outerToInner;

		// pentagon 五边形
        public const float tan36 = 0.726542528f;
		public const float sin36 = 0.587785252f;
		public const float outerRadius5 = outerRadius * 0.5f / sin36;
		public const float innerRadius5 = outerRadius * 0.5f / tan36;

		public const float elevationStep = 3f;
		public const float elevationPerturbStrength = 1.5f;
		public const float streamBedElevationOffset = -1.75f; 
		public const float waterElevationOffset = -0.5f;

		public const float solidFactor = 0.8f;
		public const float blendFactor = 1f - solidFactor; 
		public const float waterFactor = 0.6f; 
		public const float waterBlendFactor = 1f - waterFactor;

		public const float cellPerturbStrength = 4f;
		public const float noiseScale = 0.003f;

		public const int terracesPerSlope = 2;
		public const int terraceSteps = terracesPerSlope * 2 + 1; // 奇变偶不变
		public const float horizontalTerraceStepSize = 1f / terraceSteps;
		public const float verticalTerraceStepSize = 1f / (terracesPerSlope + 1);

		public const int hashGridSize = 256;
		public const float hashGridScale = 0.25f;

		static HexHash[] hashGrid;

		public static Color[] colors;

		public static void InitializeHashGrid(int seed)
		{
			hashGrid = new HexHash[hashGridSize * hashGridSize];
			Random.State currentState = Random.state;
			Random.InitState(seed);
			for (int i = 0; i < hashGrid.Length; i++)
			{
				hashGrid[i] = HexHash.Create();
			}
			Random.state = currentState;
		}

		public static Vector3[] corners = {
			new Vector3(0f, 0f, outerRadius),
			new Vector3(innerRadius, 0f, 0.5f * outerRadius),
			new Vector3(innerRadius, 0f, -0.5f * outerRadius),
			new Vector3(0f, 0f, -outerRadius),
			new Vector3(-innerRadius, 0f, -0.5f * outerRadius),
			new Vector3(-innerRadius, 0f, 0.5f * outerRadius),
			new Vector3(0f, 0f, outerRadius)
		};

		public static Texture2D noiseSource;

		public static Vector3 GetFirstCorner(HexDirection direction)
		{
			return corners[(int)direction];
		}

		public static Vector3 GetSecondCorner(HexDirection direction)
		{
			return corners[(int)direction + 1];
		}

		public static Vector3 GetFirstSolidCorner(HexDirection direction)
		{
			return corners[(int)direction] * solidFactor;
		}

		public static Vector3 GetSecondSolidCorner(HexDirection direction)
		{
			return corners[(int)direction + 1] * solidFactor;
		}
		public static Vector3 GetFirstWaterCorner(HexDirection direction)
		{
			return corners[(int)direction] * waterFactor;
		}

		public static Vector3 GetSecondWaterCorner(HexDirection direction)
		{
			return corners[(int)direction + 1] * waterFactor;
		}

		// 避免边缘错误混色，获取桥接区域
		public static Vector3 GetBridge(HexDirection direction)
		{
			return (corners[(int)direction] + corners[(int)direction + 1]) * blendFactor;
		}

		public static Vector3 GetWaterBridge(HexDirection direction)
		{
			return (corners[(int)direction] + corners[(int)direction + 1]) *
				waterBlendFactor;
		}

		public static Vector3 TerraceLerp(Vector3 a, Vector3 b, int step)
		{
			float h = step * horizontalTerraceStepSize;
			a.x += (b.x - a.x) * h;
			a.z += (b.z - a.z) * h;
			float v = ((step + 1) / 2) * verticalTerraceStepSize;
			a.y += (b.y - a.y) * v;
			return a;
		}

		public static Color TerraceLerp(Color a, Color b, int step)
		{
			float h = step * horizontalTerraceStepSize;
			return Color.Lerp(a, b, h);
		}

		public static HexEdgeType GetEdgeType(int elevation1, int elevation2)
		{
			if (elevation1 == elevation2)
			{
				return HexEdgeType.Flat;
			}
			int delta = elevation2 - elevation1;
			if (delta == 1 || delta == -1)
			{
				return HexEdgeType.Slope;
			}
			return HexEdgeType.Cliff;
		}

		public static Vector4 SampleNoise(Vector3 position)
		{
			return noiseSource.GetPixelBilinear(
				position.x * noiseScale,
				position.z * noiseScale
			);
		}

		public static Vector3 GetSolidEdgeMiddle(HexDirection direction)
		{
			return
				(corners[(int)direction] + corners[(int)direction + 1]) *
				(0.5f * solidFactor);
		}

		// 扰动顶点
		public static Vector3 Perturb(Vector3 position)
		{
			Vector4 sample = SampleNoise(position);
			position.x += (sample.x * 2f - 1f) * cellPerturbStrength;
			// 是否扰动顶部平面
			//position.y += (sample.y * 2f - 1f) * HexMetrics.cellPerturbStrength;
			position.z += (sample.z * 2f - 1f) * cellPerturbStrength;
			return position;
		}

		public static HexHash SampleHashGrid(Vector3 position)
		{
			int x = (int)(position.x * hashGridScale) % hashGridSize;
			if (x < 0)
			{
				x += hashGridSize;
			}
			int z = (int)(position.z * hashGridScale) % hashGridSize;
			if (z < 0)
			{
				z += hashGridSize;
			}
			return hashGrid[x + z * hashGridSize];
		}

	}

	public enum HexEdgeType
	{
		Flat, Slope, Cliff
	}

	public struct HexHash
	{

		public float a, b;

		public static HexHash Create()
		{
			HexHash hash;
			hash.a = Random.value;
			hash.b = Random.value;
			return hash;
		}
	}

}
