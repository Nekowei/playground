﻿using Nekowei.PlayGround;
using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Nekowei.Catlike
{
    public class HexGrid : MonoBehaviour
    {
        public int cellViewDistance;
        private int chunkViewDistance;

        public HexCell cellPrefab;
        public HexGridChunk chunkPrefab;

        Dictionary<HexCoordinates, HexCell> cells;
        Dictionary<Vector3Int, HexGridChunk> chunks;

        public EventCenterSO _ec;

        void Awake()
        {
            chunks = new Dictionary<Vector3Int, HexGridChunk>();
            cells = new Dictionary<HexCoordinates, HexCell>();
            chunkViewDistance = Mathf.CeilToInt((float)cellViewDistance / ((HexMetrics.chunkSizeX + HexMetrics.chunkSizeZ) / 2));
            CreateMap();
        }

        void OnEnable()
        {
            _ec.hexCoordChangeEvent.AddListener(OnHexCoordChange);
        }

        void OnDisable()
        {
            _ec.hexCoordChangeEvent.RemoveListener(OnHexCoordChange);
        }

        public void CreateMap()
        {
            if (chunks != null)
            {
                foreach (var c in chunks)
                {
                    Destroy(c.Value.gameObject);
                }
            }

            CreateChunks(Vector3.zero);
        }

        private void CreateLake()
        {
            //HexCell c = cells[Random.Range(0, cells.Length - 1)];
            //for (int i = 0; i < Random.Range(9, 16); i++)
            //{
            //    c = CreateRiverNext(c);
            //    if (c == null)
            //    {
            //        break;
            //    }
            //}
        }

        private void CreateSea(int seaLevel)
        {
            foreach (var c in cells)
            {
                c.Value.WaterLevel = seaLevel;
            }
        }

        private void CreateRoads()
        {
            foreach (var c in cells)
            {
                c.Value.RemoveRoads();
            }
        }

        public void CreateRiver()
        {
            //HexCell c = cells[Random.Range(0, cells.Length - 1)];
            //for (int i = 0; i < Random.Range(9, 16); i++)
            //{
            //    c = CreateRiverNext(c);
            //    if (c == null)
            //    {
            //        break;
            //    }
            //}
        }

        private HexCell CreateRiverNext(HexCell start)
        {
            List<HexDirection> ds = new List<HexDirection>();
            for (int i = 0; i < 6; i++)
            {
                HexDirection d = (HexDirection)i;
                HexCell n = start.GetNeighbor(d);
                if (n != null && !n.HasRiver)
                {
                    ds.Add(d);
                }
            }
            if (ds.Count > 0)
            {
                //HexDirection d = ds[Random.Range(0, ds.Count - 1)];
                //start.SetOutgoingRiver(d);
                //return start.GetNeighbor(d);
            }
            return null;
        }

        internal void RollElevation()
        {
            foreach (var c in chunks)
            {
                c.Value.RollElevation();
            }
        }

        void CreateChunks(Vector3 pos)
        {
            Vector3Int cc = HexCoordinates.FromPosition(pos).ToChunkCoordinates();
            for (int z = cc.z - chunkViewDistance; z <= cc.z + chunkViewDistance; z++)
            {
                for (int x = cc.x - chunkViewDistance; x <= cc.x + chunkViewDistance; x++)
                {
                    Vector3Int v = new Vector3Int(x, 0, z);
                    if (Vector3.Distance(cc, v) < chunkViewDistance)
                    {
                        CreateChunk(v); // under circle radius
                    }

                }
            }

        }

        private HexGridChunk CreateChunk(Vector3Int coordinate)
        {
            Debug.Log("create chunk:" + coordinate);
            HexGridChunk chunk = Instantiate(chunkPrefab);
            chunk.transform.SetParent(transform);
            chunk.coordinate = coordinate;
            chunks.Add(chunk.coordinate, chunk);
            CreateCells(chunk);
            return chunk;
        }

        internal void RefreshAll()
        {
            foreach (var c in chunks)
            {
                c.Value.Refresh();
            }
        }

        private void CreateCells(HexGridChunk chunk)
        {
            int sizeX = HexMetrics.chunkSizeX;
            int sizeZ = HexMetrics.chunkSizeZ;
            Vector3Int cc = chunk.coordinate;
            int cx = cc.x + sizeX / 2;
            int cz = cc.z + sizeZ / 2;
            for (int z = cc.z * sizeZ; z < (cc.z + 1) * sizeZ; z++)
            {
                for (int x = cc.x * sizeX; x < (cc.x + 1) * sizeX; x++)
                {
                    HexCell cell = CreateCell(x, z);
                    cells.Add(cell.coordinates, cell);
                    chunk.AddCell(cell);
                    if (cx == x && cz == z)
                    {
                        chunk.Position = cell.Position;
                    }
                    //CreateRoads();
                    //CreateRiver();
                    //CreateSea(MapManager.seaLevel);
                    //CreateLake();
                }
            }
        }

        HexCell CreateCell(int x, int z)
        {
            Vector3 position;
            position.x = (x + z * 0.5f - z / 2) * (HexMetrics.innerRadius * 2f);
            position.y = 0f;
            position.z = z * HexMetrics.outerRadius * 1.5f;

            HexCell cell = Instantiate(cellPrefab);
            cell.transform.localPosition = position;
            cell.coordinates = HexCoordinates.FromOffsetCoordinates(x, z);
            foreach (HexDirection dir in Enum.GetValues(typeof(HexDirection)))
            {
                if (cells.TryGetValue(cell.coordinates.GetNeighbor(dir), out HexCell n))
                {
                    cell.SetNeighbor(dir, n);
                }
            }
            cell.RollElevation();
            return cell;
        }

        /// <summary>
        /// 根据cell的x，z坐标，获取对应的chunk
        /// </summary>
        /// <returns>返回chunk的x，z坐标</returns>
        public Vector3Int GetChunk(HexCoordinates hc, out HexGridChunk chunk)
        {
            int chunkX = hc.X / HexMetrics.chunkSizeX;
            int chunkZ = hc.Z / HexMetrics.chunkSizeZ;
            chunks.TryGetValue(new Vector3Int(chunkX, 0, chunkZ), out HexGridChunk c);
            chunk = c;
            return new Vector3Int(chunkX, 0, chunkZ);
        }

        public void Save(BinaryWriter writer)
        {
            foreach (var cell in cells)
            {
                cell.Value.Save(writer);
            }
        }

        public void Load(BinaryReader reader)
        {
            foreach (var cell in cells)
            {
                cell.Value.Load(reader);
            }
            foreach (var c in chunks)
            {
                c.Value.Refresh();
            }
        }

        private void OnHexCoordChange(HexCoordinates hc, HexCoordinates newhc)
        {
            GetChunk(hc, out HexGridChunk chunk);
            Vector3Int cc = GetChunk(newhc, out HexGridChunk newChunk);
            if (chunk.Equals(newChunk))
            {
                return;
            }
            // activate chunk if inside of range
            for (int z = cc.z - chunkViewDistance; z <= cc.z + chunkViewDistance; z++)
            {
                for (int x = cc.x - chunkViewDistance; x <= cc.x + chunkViewDistance; x++)
                {
                    Vector3Int cv = new Vector3Int(x, 0, z);
                    if (!chunks.ContainsKey(cv))
                    {
                        HexGridChunk newGc = CreateChunk(cv);
                        newGc.Refresh();
                    }
                }
            }
            toggleCell(hc);
        }

        private void toggleCell(HexCoordinates hc)
        {
            foreach (HexCell cell in cells.Values)
            {
                // toggle cell active by view distance
                bool isNear = hc.Distance(cell.coordinates) <= cellViewDistance;
                if (cell.gameObject.activeSelf != isNear)
                {
                    cell.gameObject.SetActive(isNear);
                    cell.chunk.Refresh();
                }
            }
        }


    }
}

