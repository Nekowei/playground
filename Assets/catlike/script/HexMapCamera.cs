﻿using Nekowei.PlayGround;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Nekowei.Catlike
{
	public class HexMapCamera : MonoBehaviour
	{
		Transform swivel, stick;

		float zoom = 1f;
		float rotationAngle;

		public Camera hexCamera;
		public float stickMinZoom, stickMaxZoom;
		public float swivelMinZoom, swivelMaxZoom;
		public float moveSpeedMinZoom, moveSpeedMaxZoom;
		public float rotationSpeed;
		[Range(1, 10)]
		public int zoomSpeed;

		public HexGrid hexGrid { get; set; }

		public VoidEventSO cameraToggleEvent;

		void Awake()
		{
			swivel = transform.GetChild(0);
			stick = swivel.GetChild(0);
			enabled = false;
			cameraToggleEvent.action += OnCameraToggle;
		}

		void OnEnable()
        {
			if (hexCamera)
            {
				hexCamera.enabled = true;
            }
        }

		void OnDisable()
        {
			if (hexCamera)
            {
				hexCamera.enabled = false;
            }
		}

		void OnDestroy()
		{
			cameraToggleEvent.action -= OnCameraToggle;
		}

		private void OnCameraToggle() { enabled = !enabled; }

		void Update()
		{
			float zoomDelta = Mouse.current.scroll.ReadValue().y;
			if (zoomDelta != 0f)
			{
				AdjustZoom(zoomDelta / 120 * zoomSpeed * Time.deltaTime);
			}

			if (Keyboard.current.qKey.isPressed)
            {
				AdjustRotation(1);
			}
			else if (Keyboard.current.eKey.isPressed)
            {
				AdjustRotation(-1);
			}

			// TODO 改为new input system，根据camera 视角方向操控而不是空间绝对方向
			float xDelta = Input.GetAxis("Horizontal");
			float zDelta = Input.GetAxis("Vertical");
			if (xDelta != 0f || zDelta != 0f)
			{
				AdjustPosition(xDelta, zDelta);
			}
		}

		void AdjustZoom(float delta)
		{
			zoom = Mathf.Clamp01(zoom + delta);

			float distance = Mathf.Lerp(stickMinZoom, stickMaxZoom, zoom);
			stick.localPosition = new Vector3(0f, 0f, distance);

			float angle = Mathf.Lerp(swivelMinZoom, swivelMaxZoom, zoom);
			swivel.localRotation = Quaternion.Euler(angle, 0f, 0f);
		}

		void AdjustPosition(float xDelta, float zDelta)
		{
			Vector3 direction = new Vector3(xDelta, 0f, zDelta).normalized;
			float damping = Mathf.Max(Mathf.Abs(xDelta), Mathf.Abs(zDelta));
			float distance = Mathf.Lerp(moveSpeedMinZoom, moveSpeedMaxZoom, zoom) * damping * Time.deltaTime;

			Vector3 position = transform.localPosition;
			position += direction * distance;
			transform.localPosition = ClampPosition(position);
		}

		Vector3 ClampPosition(Vector3 position)
		{
			//float xMax =
			//	(hexGrid.cellCountX * HexMetrics.chunkSizeX - 0.5f) *
			//	(2f * HexMetrics.innerRadius);
			//position.x = Mathf.Clamp(position.x, 0f, xMax);

			//float zMax =
			//	(hexGrid.cellCountZ * HexMetrics.chunkSizeZ - 1) *
			//	(1.5f * HexMetrics.outerRadius);
			//position.z = Mathf.Clamp(position.z, 0f, zMax);

			return position;
		}

		void AdjustRotation(float delta)
		{
			rotationAngle += delta * rotationSpeed * Time.deltaTime;
			if (rotationAngle < 0f)
			{
				rotationAngle += 360f;
			}
			else if (rotationAngle >= 360f)
			{
				rotationAngle -= 360f;
			}
			transform.localRotation = Quaternion.Euler(0f, rotationAngle, 0f);
		}

	}
}
