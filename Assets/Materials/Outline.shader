﻿Shader "Unlit/Outline"
{
    Properties
    {
        _MainColor("Main Color", Color)=(1,1,1,1)
        _MainTexture("Main Texture", 2D)="white"{}
        _OutlineColor("Outline Color", Color)=(1,1,1,1)
        _Outline("Outline", Range(0, 1.5))=1.1
    }

    SubShader
    {

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
            fixed4 _MainColor;
            sampler2D _MainTexture;
            float4 _MainTexture_ST;
            struct appdata
            {
                float4 vertex:POSITION;
                float2 uv:TEXCOORD0;
            };
            struct v2f
            {
                float4 clipPos:SV_POSITION;
                float2 uv:TEXCOORD0;
            };
            v2f vert (appdata v)
            {
                v2f o;
                o.clipPos=UnityObjectToClipPos(v.vertex);
                o.uv=(v.uv*_MainTexture_ST.xy)+_MainTexture_ST.zw;
                return o;
            }
            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col;
                col=tex2D(_MainTexture, i.uv)*_MainColor;
                return col;
            }
            ENDCG
        }

        //outLinePass
		Pass{
			Name "OutLine"
			cull Front //关闭模型正面（不画模型正面）

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"


			float _Outline;
			fixed4 _OutlineColor;

			struct v2f{
				float4 vertex:SV_POSITION;

			};
			
			v2f vert(appdata_base v)
			{
				v2f o;
				//=============物体空间法线外拓========
				v.vertex.xyz+=v.normal*_Outline;
				o.vertex=UnityObjectToClipPos(v.vertex);

				//=================视角空间法线外拓=================
				//float4 pos=mul(UNITY_MATRIX_V,mul(unity_ObjectToWorld,v.vertex)); //UnityObjectToViewPos(v.vertex);//顶点转到视角
				////使用物体坐标到视角坐标的转置矩阵 
				//float3 normal=normalize(mul((float3x3)UNITY_MATRIX_IT_MV,v.normal));
				//pos=pos+float4(normal,0)*_OutLine;//外拓顶点
				//o.vertex=mul(UNITY_MATRIX_P,pos);//转到裁剪空间

				//=============裁剪空间法线外拓===================
				//o.vertex=UnityObjectToClipPos(v.vertex);
				//float3 normal=normalize(mul((float3x3)UNITY_MATRIX_IT_MV,v.normal));//转到视角坐标 z值变成了纯粹的深度
				//float2 viewNormal=TransformViewToProjection(normal.xy);//转到三角空间
				//o.vertex.xy+=viewNormal*_OutLine;

				return o;
			}


			fixed4 frag(v2f i):SV_Target
			{
				return _OutlineColor;
			}

			ENDCG
		
		}
    }
}
