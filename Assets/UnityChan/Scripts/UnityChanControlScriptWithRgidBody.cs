﻿////
//// Mecanimのアニメーションデータが、原点で移動しない場合の Rigidbody付きコントローラ
//// サンプル
//// 2014/03/13 N.Kobyasahi
////
//using UnityEngine;
//using System.Collections;
//using System;
//using Nekowei.PlayGround;
//using static Nekowei.PlayGround.InputActions;
//using UnityEngine.InputSystem;

//namespace UnityChan
//{
//    // 必要なコンポーネントの列記
//    [RequireComponent(typeof(Animator))]
//    [RequireComponent(typeof(CapsuleCollider))]
//    [RequireComponent(typeof(Rigidbody))]

//    public class UnityChanControlScriptWithRgidBody : MonoBehaviour, IPlayerActions
//    {

//        public float animSpeed = 1.5f;              // アニメーション再生速度設定
//        public float lookSmoother = 3.0f;           // a smoothing setting for camera motion
//        public bool useCurves = true;               // Mecanimでカーブ調整を使うか設定する
//                                                    // このスイッチが入っていないとカーブは使われない
//        public float useCurvesHeight = 0.5f;        // カーブ補正の有効高さ（地面をすり抜けやすい時には大きくする）

//        // 以下キャラクターコントローラ用パラメタ
//        // 前進速度
//        public float forwardSpeed = 7.0f;
//        // 後退速度
//        public float backwardSpeed = 2.0f;
//        // 旋回速度
//        public float rotateSpeed = 2.0f;
//        // ジャンプ威力
//        public float jumpPower = 3.0f;
//        // キャラクターコントローラ（カプセルコライダ）の参照
//        private CapsuleCollider col;
//        private Rigidbody rb;
//        // キャラクターコントローラ（カプセルコライダ）の移動量
//        private Vector3 velocity;
//        // CapsuleColliderで設定されているコライダのHeiht、Centerの初期値を収める変数
//        private float orgColHight;
//        private Vector3 orgVectColCenter;
//        private Animator anim;                          // キャラにアタッチされるアニメーターへの参照
//        private AnimatorStateInfo currentBaseState;         // base layerで使われる、アニメーターの現在の状態の参照

//        public GameObject cameraObject;    // メインカメラへの参照
//        private TheOneCamera cameraController;

//        private Transform rotateBody;

//        // アニメーター各ステートへの参照
//        static int idleState = Animator.StringToHash("Base Layer.Idle");
//        static int locoState = Animator.StringToHash("Base Layer.Locomotion");
//        static int jumpState = Animator.StringToHash("Base Layer.Jump");
//        static int restState = Animator.StringToHash("Base Layer.Rest");

//        private InputManager input;

//        // 初期化
//        void Start()
//        {
//            input = ScriptableObject.CreateInstance<InputManager>();
//            // Animatorコンポーネントを取得する
//            anim = GetComponent<Animator>();
//            // CapsuleColliderコンポーネントを取得する（カプセル型コリジョン）
//            col = GetComponent<CapsuleCollider>();
//            rb = GetComponent<Rigidbody>();
//            //メインカメラを取得する
//            cameraController = cameraObject.GetComponent<TheOneCamera>();
//            // CapsuleColliderコンポーネントのHeight、Centerの初期値を保存する
//            orgColHight = col.height;
//            orgVectColCenter = col.center;

//            if (GameObject.Find("RotateBody"))
//                rotateBody = GameObject.Find("RotateBody").transform;

//        }

//        // 以下、メイン処理.リジッドボディと絡めるので、FixedUpdate内で処理を行う.
//        void FixedUpdate()
//        {
//            var move = input.readMove();
//            velocity = new Vector3(move.x, 0, move.y);

//            var charf = transform.forward;
//            var bodyForward = new Vector3(charf.x, 0, charf.z);

//            var angle = Vector3.Angle(bodyForward.normalized, velocity.normalized);
//            var cross = Vector3.Cross(bodyForward.normalized, velocity.normalized);
//            var dir = angle / 90.0f * (cross.y < 0 ? -1 : 1);
//            anim.SetFloat("Direction", dir);
//            anim.SetFloat("Speed", velocity.magnitude);

//            anim.speed = animSpeed;
//            currentBaseState = anim.GetCurrentAnimatorStateInfo(0); // 参照用のステート変数にBase Layer (0)の現在のステートを設定する
//            rb.useGravity = true;//ジャンプ中に重力を切るので、それ以外は重力の影響を受けるようにする

//            // 朝按键方向移动（摄像机坐标系）
//            velocity = Camera.main.transform.TransformDirection(velocity);
//            velocity *= forwardSpeed;
//            transform.position += velocity * Time.fixedDeltaTime;

//            // 朝按键方向旋转（摄像机坐标系）
//            //var camf = Camera.main.transform.forward;
//            //var camForward = new Vector3(camf.x, 0, camf.z);
//            //var camAngle = Vector3.Angle(camForward.normalized, velocity.normalized);
//            //var camCross = Vector3.Cross(camForward.normalized, velocity.normalized);
//            //var camDir = camAngle / 90.0f * (camCross.y < 0 ? -1 : 1);
//            //rotateBody.Rotate(0, camDir * rotateSpeed, 0);

//            rotateBody.LookAt(velocity.normalized);

//            // 以下、Animatorの各ステート中での処理
//            if (currentBaseState.fullPathHash == locoState)
//            {
//                //カーブでコライダ調整をしている時は、念のためにリセットする
//                if (useCurves)
//                {
//                    resetCollider();
//                }
//            }
//            else if (currentBaseState.fullPathHash == jumpState)
//            {
//                jump();
//            }
//            else if (currentBaseState.fullPathHash == idleState)
//            {
//                idle();
//            }
//            else if (currentBaseState.fullPathHash == restState)
//            {
//                //cameraObject.SendMessage("setCameraPositionFrontView");		// カメラを正面に切り替える
//                // ステートが遷移中でない場合、Rest bool値をリセットする（ループしないようにする）
//                if (!anim.IsInTransition(0))
//                {
//                    anim.SetBool("Rest", false);
//                }
//            }


//        }

//        private void idle()
//        {
//            //カーブでコライダ調整をしている時は、念のためにリセットする
//            if (useCurves)
//            {
//                resetCollider();
//            }
//            // スペースキーを入力したらRest状態になる
//            if (Input.GetButtonDown("Jump"))
//            {
//                anim.SetBool("Rest", true);
//            }
//        }

//        private void jump()
//        {
//            cameraObject.SendMessage("setCameraPositionJumpView");  // ジャンプ中のカメラに変更
//                                                                    // ステートがトランジション中でない場合
//            if (!anim.IsInTransition(0))
//            {
//                // 以下、カーブ調整をする場合の処理
//                if (useCurves)
//                {
//                    // 以下JUMP00アニメーションについているカーブJumpHeightとGravityControl
//                    // JumpHeight:JUMP00でのジャンプの高さ（0〜1）
//                    // GravityControl:1⇒ジャンプ中（重力無効）、0⇒重力有効
//                    float jumpHeight = anim.GetFloat("JumpHeight");
//                    float gravityControl = anim.GetFloat("GravityControl");
//                    if (gravityControl > 0)
//                        rb.useGravity = false;  //ジャンプ中の重力の影響を切る

//                    // レイキャストをキャラクターのセンターから落とす
//                    Ray ray = new Ray(transform.position + Vector3.up, -Vector3.up);
//                    RaycastHit hitInfo = new RaycastHit();
//                    // 高さが useCurvesHeight 以上ある時のみ、コライダーの高さと中心をJUMP00アニメーションについているカーブで調整する
//                    if (Physics.Raycast(ray, out hitInfo))
//                    {
//                        if (hitInfo.distance > useCurvesHeight)
//                        {
//                            col.height = orgColHight - jumpHeight;          // 調整されたコライダーの高さ
//                            float adjCenterY = orgVectColCenter.y + jumpHeight;
//                            col.center = new Vector3(0, adjCenterY, 0); // 調整されたコライダーのセンター
//                        }
//                        else
//                        {
//                            // 閾値よりも低い時には初期値に戻す（念のため）					
//                            resetCollider();
//                        }
//                    }
//                }
//                // Jump bool値をリセットする（ループしないようにする）				
//                anim.SetBool("Jump", false);
//            }
//        }

//        // キャラクターのコライダーサイズのリセット関数
//        void resetCollider()
//        {
//            // コンポーネントのHeight、Centerの初期値を戻す
//            col.height = orgColHight;
//            col.center = orgVectColCenter;
//        }

//        public void OnMove(InputAction.CallbackContext context)
//        {
//        }

//        public void OnFire(InputAction.CallbackContext context)
//        {
//        }

//        public void OnJump(InputAction.CallbackContext context)
//        {
//            //アニメーションのステートがLocomotionの最中のみジャンプできる
//            if (currentBaseState.fullPathHash == locoState)
//            {
//                //ステート遷移中でなかったらジャンプできる
//                if (!anim.IsInTransition(0))
//                {
//                    rb.AddForce(Vector3.up * jumpPower, ForceMode.VelocityChange);
//                    anim.SetBool("Jump", true);     // Animatorにジャンプに切り替えるフラグを送る
//                }
//            }
//        }

//        public void OnLook(InputAction.CallbackContext context)
//        {
//            var look = input.readLook();
//            cameraController.updateAngle(look);
//        }

//        void OnCollisionEnter(Collision collision)
//        {
//            var obj = collision.collider.gameObject;
//            if (obj.tag.Equals("collect"))
//            {
//                Debug.Log(obj.GetComponent<CollectController>().type);
//                Destroy(obj);
//            }

//        }

//    }
//}