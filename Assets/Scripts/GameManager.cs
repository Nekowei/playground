using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Nekowei.Castle;
using Nekowei.Catlike;

namespace Nekowei.PlayGround
{
    public class GameManager : MonoBehaviour
    {
        public GameObject buildManagerPrefab;
        public GameObject mapManagerPrefab;

        private GameObject buildManager;
        private GameObject mapManager;

        void Start()
        {
            buildManager = Instantiate(buildManagerPrefab);
            mapManager = Instantiate(mapManagerPrefab);
        }

        public void Save()
        {
            mapManager.GetComponent<MapManager>().Save();
        }

        public void Load()
        {
            mapManager.GetComponent<MapManager>().Load();
        }

    }

}
