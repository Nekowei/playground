﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectController : MonoBehaviour
{

    public float rotateSpeed = 10;
    public float floatHeight = 10;

    public enum CollectType { coin, badge }

    public CollectType type;

    private Vector3 startPosition;

    public void Start()
    {
        startPosition = transform.parent.position;
    }

    public void FixedUpdate()
    {
        var parent = transform.parent.transform;
        parent.Rotate(transform.parent.up, rotateSpeed * Time.fixedDeltaTime);
        transform.position = new Vector3(
            startPosition.x, 
            startPosition.y + floatHeight * Mathf.Sin(Time.realtimeSinceStartup) * Time.fixedDeltaTime, 
            startPosition.z);
    }

}
