﻿//using Nekowei.PlayGround;
//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using UnityEngine.InputSystem;
//using static Nekowei.PlayGround.InputActions;

//namespace Nekowei.PlayGround {
//    public class InputManager : ScriptableObject
//    {
//        private float mouseRotateModifier = 0.05f;
//        private static InputActions actions;
//        public void setAction(IPlayerActions action)
//        {
//            actions.Player.SetCallbacks(action);
//            actions.Player.Move.performed += action.OnMove;
//        }

//        public void OnEnable()
//        {
//            if (actions == null)
//            {
//                actions = new InputActions();
//            }
//            actions.Player.Enable();
//        }

//        public void OnDisable()
//        {
//            actions.Player.Disable();
//        }

//        public Vector2 readMove()
//        {
//            var rua = Vector2.zero;
//            var move = actions.Player.Move;
//            if (move.activeControl != null
//                && move.activeControl.device is Gamepad
//                && move.activeControl.IsPressed())
//            {
//                rua = move.ReadValue<Vector2>();
//            }
//            else
//            {
//                if (Keyboard.current.wKey.isPressed)
//                {
//                    rua.y += 1;
//                }
//                if (Keyboard.current.sKey.isPressed)
//                {
//                    rua.y -= 1;
//                }
//                if (Keyboard.current.dKey.isPressed)
//                {
//                    rua.x += 1;
//                }
//                if (Keyboard.current.aKey.isPressed)
//                {
//                    rua.x -= 1;
//                }
//            }
//            return rua;
//        }

//        public Vector2 readLook()
//        {
//            Vector2 rua;
//            var look = actions.Player.Look;
//            if (look.activeControl != null
//                && look.activeControl.device is Gamepad
//                && look.activeControl.IsPressed())
//            {
//                rua = look.ReadValue<Vector2>();
//            }
//            else
//            {
//                var raw = Mouse.current.delta.ReadValue();
//                rua = raw * mouseRotateModifier;
//            }
//            return rua;
//        }

//    }

//}
