using System;
using UnityEngine;

namespace Nekowei.Util
{
    public class MaterialUtil
    {
        public static Material[] RemoveMaterial(Material[] ms, Material prefab)
        {
            int i;
            if ((i = IndexOf(ms, prefab)) > -1)
            {
                ms[i] = null;
            }
            return ms;
        }

        public static Material[] AddMaterial(Material[] ms, Material prefab)
        {
            if (IndexOf(ms, prefab) > -1)
            {
                return ms;
            }
            else if (ms[ms.Length - 1] == null)
            {
                ms[ms.Length - 1] = prefab;
                return ms;
            }
            else
            {
                Material[] newms = new Material[ms.Length + 1];
                ms.CopyTo(newms, 0);
                newms[ms.Length] = prefab;
                return newms;
            }
        }

        public static int IndexOf(Material[]ms, Material m)
        {
            for (int i = 0; i < ms.Length; i++)
            {
                if (ms[i] != null && ms[i].Equals(m))
                {
                    return i; 
                }
            }
            return -1;
        }
    }
}
