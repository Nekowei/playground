﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using Nekowei.Util;
using System;
using static Nekowei.Util.MaterialUtil;
using Nekowei.PlayGround;

namespace Nekowei.Castle
{
    public enum BuildMode
    {
        none, build, delete
    }

    public class BuildManager : MonoBehaviour
    {

        private BuildMode buildMode = BuildMode.none;

        private GameObject showModel;
        public GameObject[] brickPrefab;
        private GameObject choosenPrefab;
        private GameObject lastChoosen;

        public Material buildModeMaterial;
        public Material deleteModeMaterial;

        public VoidEventSO buildButtonClickEvent;
        public IntEventSO buildPanelButtonClickEvent;

        private List<GameObject> entityList = new List<GameObject>();

        void OnEnable()
        {
            buildButtonClickEvent.action += OnBuildButtonClick;
            buildPanelButtonClickEvent.action += OnBuildPanelButtonClick;
        }

        void OnDisable()
        {
            buildButtonClickEvent.action -= OnBuildButtonClick;
            buildPanelButtonClickEvent.action -= OnBuildPanelButtonClick;
        }

        void Update()
        {
            if (buildMode.Equals(BuildMode.build) && !UIEventUtil.IsPointerOverUIObject())
            {
                UpdateBuild();
            }
            else if (buildMode.Equals(BuildMode.delete) && !UIEventUtil.IsPointerOverUIObject())
            {
                UpdateDelete();
            }
        }

        private void UpdateBuild()
        {
            if (showModel == null)
            {
                showModel = Instantiate(choosenPrefab);
                showModel.GetComponent<Rigidbody>().detectCollisions = false;
                showModel.GetComponent<MeshRenderer>().material = buildModeMaterial;
            }
            if (Keyboard.current.rKey.wasReleasedThisFrame)
            {
                showModel.transform.Rotate(Vector3.up, 90);
            }
            Ray ray = Camera.main.ScreenPointToRay(Mouse.current.position.ReadValue());
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                var pos = hit.point;
                showModel.transform.position = pos;
                if (Mouse.current.leftButton.wasReleasedThisFrame)
                {
                    entityList.Add(Instantiate(choosenPrefab, pos, showModel.transform.rotation));
                }
            }
        }

        private void UpdateDelete()
        {
            showModel = null;
            Ray ray = Camera.main.ScreenPointToRay(Mouse.current.position.ReadValue());
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                GameObject collider = hit.collider.gameObject;
                if (collider.tag.Equals("brick"))
                {
                    if (!collider.Equals(lastChoosen))
                    {
                        if (lastChoosen != null)
                        {
                            lastChoosen.GetComponent<MeshRenderer>().sharedMaterials = RemoveMaterial(
                                lastChoosen.GetComponent<MeshRenderer>().sharedMaterials, deleteModeMaterial);
                        }
                        collider.GetComponent<MeshRenderer>().sharedMaterials = AddMaterial(
                            collider.GetComponent<MeshRenderer>().sharedMaterials, deleteModeMaterial);
                        lastChoosen = collider;
                    }
                    if (Mouse.current.leftButton.wasReleasedThisFrame)
                    {
                        Destroy(collider);
                    }
                }
            }
        }

        public void ToggleBuildMode(bool enableBuildMode)
        {
            buildMode = enableBuildMode ? BuildMode.build : BuildMode.none;
        }

        public void ChoosePrefab(int i)
        {
            choosenPrefab = brickPrefab[i];
            buildMode = BuildMode.build;
            showModel = null;
        }

        private void OnBuildButtonClick()
        {
            if (buildMode.Equals(BuildMode.none))
            {
                buildMode = BuildMode.build;
            }
            else
            {
                buildMode = BuildMode.none;
            }
        }

        private void OnBuildPanelButtonClick(int i)
        {
            if (i.Equals(-1))
            {
                buildMode = BuildMode.delete;
            }
            else
            {
                ChoosePrefab(i);
            }
        }


    }
}
