using Nekowei.Catlike;
using Nekowei.PlayGround;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityChan;
using Nekowei.Util;

namespace Nekowei.Castle
{
    public class UIManager : MonoBehaviour
    {

        private GameObject mapPanel;
        private GameObject buildPanel;

        public void Start()
        {
            mapPanel = transform.Find("MapPanel").gameObject;
            buildPanel = transform.Find("BuildPanel").gameObject;
            mapPanel.SetActive(false);
            buildPanel.SetActive(false);
        }

        public void ToggleWorldPanel()
        {
            mapPanel.SetActive(!mapPanel.activeSelf);
        }

        public void ToggleBuildPanel()
        {
            buildPanel.SetActive(!buildPanel.activeSelf);
        }

    }
}
