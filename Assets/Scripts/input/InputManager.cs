﻿using System;
using UnityEngine;
using UnityEngine.InputSystem;
using static Nekowei.PlayGround.InputActions;
using static UnityEngine.InputSystem.InputAction;

namespace Nekowei.PlayGround
{
    [CreateAssetMenu(menuName = "Game/Input Reader")]
    public class InputManager : ScriptableObject, IPlayerActions
    {
        public Action<CallbackContext> moveStarted;
        public Action<CallbackContext> moveCanceled;
        public Action<Vector2> look;
        public Action<Vector2> zoom;

        public InputActions actions;

        private Vector2 lastMoveInput;

        public void OnEnable()
        {
            if (actions == null)
            {
                actions = new InputActions();
                actions.Player.SetCallbacks(this);
            }
            actions.Player.Enable();
        }

        public void OnDisable()
        {
            actions.Player.Disable();
        }

        public void OnFire(CallbackContext context)
        {
            
        }

        public void OnJump(CallbackContext context)
        {
            
        }

        public void OnLook(CallbackContext ctx)
        {
            if (ctx.control.device is Gamepad || 
                (ctx.control.device is Mouse && Mouse.current.rightButton.isPressed))
            {
                look.Invoke(ctx.ReadValue<Vector2>());
            }
        }

        public void OnMove(CallbackContext ctx)
        {
            if (ctx.started)
            {
                moveStarted.Invoke(ctx);
            }
            if (ctx.canceled)
            {
                moveCanceled.Invoke(ctx);
            }
        }

        public void OnZoom(CallbackContext ctx)
        {
            zoom.Invoke(ctx.ReadValue<Vector2>());
        }
    }
}
