﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using Nekowei.Catlike;
using static UnityEngine.InputSystem.InputAction;

namespace Nekowei.PlayGround
{
    [RequireComponent(typeof(Animator))]
    [RequireComponent(typeof(CapsuleCollider))]
    [RequireComponent(typeof(Rigidbody))]
    public class TheOneController : MonoBehaviour
    {

        public float speed;
        public float jumpForce;
        public float maxSpeed;
        public int maxJumpCount;
        public float jumpCD;
        private int jumpCount;
        private double lastJumpTime;
        private bool isMoving = false;

        private Rigidbody rb;
        private GameObject theOneCameraGO;
        private TheOneCamera theOneCamera;
        private Transform gameplayCamera;
        private TheOneAnimation theOneAnimation;
        private Transform rotateBody;

        public InputManager input = default;

        public VoidEventSO cameraToggleEvent;
        public EventCenterSO _ec;

        private HexCoordinates hexCoord;

        void Awake()
        {
            rb = GetComponent<Rigidbody>();
            theOneCameraGO = transform.Find("TheOneCamera").gameObject;
            theOneCamera = theOneCameraGO.GetComponent<TheOneCamera>();
            gameplayCamera = theOneCameraGO.transform;
            theOneAnimation = GetComponent<TheOneAnimation>();
            rotateBody = transform.Find("RotateBody");
        }

        void Start()
        {
            cameraToggleEvent.action += OnCameraToggle;
        }

        void OnEnable()
        {
            input.moveStarted += moveStarted;
            input.moveCanceled += moveCanceled;
            input.look += theOneCamera.look;
            input.zoom += theOneCamera.zoom;
        }

        void OnDisable()
        {
            input.moveStarted -= moveStarted;
            input.moveCanceled -= moveCanceled;
            input.look -= theOneCamera.look;
            input.zoom -= theOneCamera.zoom;
        }

        void OnDestroy()
        {
            cameraToggleEvent.action -= OnCameraToggle;
        }

        public void Update()
        {
            if (isMoving)
            {
                Vector2 m = input.actions.Player.Move.ReadValue<Vector2>();
                rb.velocity = RecalculateMovement(m) * speed;
                rotateBody.LookAt(rotateBody.position + rb.velocity);
            }
            if (rb.velocity.magnitude > maxSpeed)
            {
                rb.velocity = rb.velocity.normalized * maxSpeed;
            }
            theOneAnimation.velocity = rb.velocity;
            var newHexCoord = HexCoordinates.FromPosition(transform.position);
            if (!hexCoord.Equals(newHexCoord))
            {
                _ec.hexCoordChangeEvent.Invoke(hexCoord, newHexCoord);
                hexCoord = newHexCoord;
            }
        }

        private Vector3 RecalculateMovement(Vector2 input)
        {
            //Get the two axes from the camera and flatten them on the XZ plane
            Vector3 cameraForward = gameplayCamera.forward;
            cameraForward.y = 0f;
            Vector3 cameraRight = gameplayCamera.right;
            cameraRight.y = 0f;

            //Use the two axes, modulated by the corresponding inputs, and construct the final vector
            Vector3 adjustedMovement = cameraRight.normalized * input.x +
                cameraForward.normalized * input.y;

            return Vector3.ClampMagnitude(adjustedMovement, 1f);
        }

        public void OnJump(CallbackContext context)
        {
            if (context.ReadValueAsButton())
            {
                if (jumpCount < maxJumpCount 
                    && context.startTime - lastJumpTime > jumpCD)
                {
                    rb.AddForce(Vector3.up * jumpForce * (jumpCount+1));
                    jumpCount++;
                    lastJumpTime = context.startTime;
                }
            }
        }

        private void OnCameraToggle()
        {
            enabled = !enabled;
        }

        private void moveStarted(CallbackContext ctx)
        {
            isMoving = true;
        }

        private void moveCanceled(CallbackContext ctx)
        {
            isMoving = false;
            rb.velocity = Vector3.zero;
        }
    }
}

