﻿using Nekowei.Util;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Nekowei.PlayGround
{
    public class TheOneCamera : MonoBehaviour
    {

        public float smooth = 3f;
        public float rotateSpeed = 20;

        public Transform camFocus;
        bool bQuickSwitch = false;

        public float minRotationY = 5;
        public float maxRotationY = 80;
        private float minRotationYpi;
        private float maxRotationYpi;
        private float distance = 3;
        private Vector2 lastRotPos = Vector2.zero;

        public float minDistance = 2;
        public float maxDistance = 5;
        public float zoomSpeed = 1;

        private Vector2 angle = new Vector2(0, 15);

        private bool change = false;

        public VoidEventSO cameraToggleEvent;

        void Start()
        {
            cameraToggleEvent.action += OnCameraToggle;

            minRotationYpi = minRotationY / 180 * Mathf.PI;
            maxRotationYpi = maxRotationY / 180 * Mathf.PI;
        }

        void OnEnable()
        {
            GetComponent<Camera>().enabled = true;
        }

        void OnDisable()
        {
            GetComponent<Camera>().enabled = false;
        }

        void OnDestroy()
        {
            cameraToggleEvent.action -= OnCameraToggle;
        }

        private void OnCameraToggle() { enabled = !enabled; }

        void Update()
        {
            Vector3 normal = new Vector3(Mathf.Sin(angle.x), Mathf.Sin(angle.y), Mathf.Cos(angle.x));
            transform.position = Vector3.Lerp(transform.position, camFocus.position + normal.normalized * distance, smooth);
            transform.LookAt(camFocus);
        }
        
        public void look(Vector2 d)
        {
            float dx = (d.x - lastRotPos.x);
            float dy = (d.y - lastRotPos.y);
            angle += new Vector2(dx, dy) * rotateSpeed * Time.deltaTime;
            if (angle.x > 2 * Mathf.PI)
            {
                angle.x = 0;
            }
            if (angle.x < 0)
            {
                angle.x = 2 * Mathf.PI;
            }
            if (angle.y > maxRotationYpi)
            {
                angle.y = maxRotationYpi;
            }
            if (angle.y < minRotationYpi)
            {
                angle.y = minRotationYpi;
            }

            lastRotPos = d;
        }

        public void zoom(Vector2 d)
        {
            distance -= d.y * zoomSpeed / 100 * Time.deltaTime;
            if (distance < minDistance)
            {
                distance = minDistance;
            }
            if (distance > maxDistance)
            {
                distance = maxDistance;
            }
        }

    }
}

