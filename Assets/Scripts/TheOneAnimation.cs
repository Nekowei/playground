using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class TheOneAnimation : MonoBehaviour
{
    public float animSpeed = 1.5f;              // アニメーション再生速度設定
    public float lookSmoother = 3.0f;           // a smoothing setting for camera motion
    public bool useCurves = true;               // Mecanimでカーブ調整を使うか設定する
                                                // このスイッチが入っていないとカーブは使われない
    public float useCurvesHeight = 0.5f;        // カーブ補正の有効高さ（地面をすり抜けやすい時には大きくする）

    // キャラクターコントローラ（カプセルコライダ）の移動量
    public Vector3 velocity;

    private Animator anim;                          // キャラにアタッチされるアニメーターへの参照
    private AnimatorStateInfo currentBaseState;         // base layerで使われる、アニメーターの現在の状態の参照

    // アニメーター各ステートへの参照
    static int idleState = Animator.StringToHash("Base Layer.Idle");
    static int locoState = Animator.StringToHash("Base Layer.Locomotion");
    static int jumpState = Animator.StringToHash("Base Layer.Jump");
    static int restState = Animator.StringToHash("Base Layer.Rest");

    // 初期化
    void Start()
    {
        // Animatorコンポーネントを取得する
        anim = GetComponent<Animator>();
    }

    // 以下、メイン処理.リジッドボディと絡めるので、FixedUpdate内で処理を行う.
    void FixedUpdate()
    {
        //var charf = transform.forward;
        //var bodyForward = new Vector3(charf.x, 0, charf.z);
        //var angle = Vector3.Angle(bodyForward.normalized, velocity.normalized);
        //var cross = Vector3.Cross(bodyForward.normalized, velocity.normalized);
        //var dir = angle / 90.0f * (cross.y < 0 ? -1 : 1);
        //anim.SetFloat("Direction", dir);
        anim.SetFloat("Speed", velocity.magnitude);

        anim.speed = animSpeed;
        currentBaseState = anim.GetCurrentAnimatorStateInfo(0); // 参照用のステート変数にBase Layer (0)の現在のステートを設定する
        // 以下、Animatorの各ステート中での処理
        if (currentBaseState.fullPathHash == locoState)
        {
        }
        else if (currentBaseState.fullPathHash == jumpState)
        {
            jump();
        }
        else if (currentBaseState.fullPathHash == idleState)
        {
            idle();
        }
        else if (currentBaseState.fullPathHash == restState)
        {
            //cameraObject.SendMessage("setCameraPositionFrontView");		// カメラを正面に切り替える
            // ステートが遷移中でない場合、Rest bool値をリセットする（ループしないようにする）
            if (!anim.IsInTransition(0))
            {
                anim.SetBool("Rest", false);
            }
        }

    }

    private void idle()
    {
        // スペースキーを入力したらRest状態になる
        if (Input.GetButtonDown("Jump"))
        {
            anim.SetBool("Rest", true);
        }
    }

    private void jump()
    {
        if (!anim.IsInTransition(0))
        {
            // Jump bool値をリセットする（ループしないようにする）				
            anim.SetBool("Jump", false);
        }
    }

    public void OnJump(InputAction.CallbackContext context)
    {
        //アニメーションのステートがLocomotionの最中のみジャンプできる
        if (currentBaseState.fullPathHash == locoState)
        {
            //ステート遷移中でなかったらジャンプできる
            if (!anim.IsInTransition(0))
            {
                anim.SetBool("Jump", true);     // Animatorにジャンプに切り替えるフラグを送る
            }
        }
    }

}
