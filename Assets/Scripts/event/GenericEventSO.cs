using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Nekowei.PlayGround
{
    public class GenericEventSO<T> : ScriptableObject
    {
        public Action<T> action;

        public void callEvent(T s)
        {
            if (action == null)
            {
                Debug.LogError(GetType().Name + ": no action found");
            }
            else
            {
                action.Invoke(s);
            }
        }
    }
}
