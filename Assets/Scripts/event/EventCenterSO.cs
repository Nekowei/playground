using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Nekowei.Catlike;

namespace Nekowei.PlayGround
{
    [CreateAssetMenu(fileName = "EventCenter", menuName = "Game/EventCenter")]
    public class EventCenterSO : ScriptableObject
    {
        public UnityEvent<HexCoordinates, HexCoordinates> hexCoordChangeEvent;

        void Awake()
        {
            hexCoordChangeEvent = new UnityEvent<HexCoordinates, HexCoordinates>();
        }
    }
}
