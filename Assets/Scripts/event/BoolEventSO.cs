using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Nekowei.PlayGround
{
    [CreateAssetMenu(menuName = "Game/EventBool")]
    public class BoolEventSO : GenericEventSO<bool>
    {
    }
}
