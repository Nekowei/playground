using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Nekowei.PlayGround
{
    [CreateAssetMenu(menuName = "Game/EventInt")]
    public class IntEventSO : GenericEventSO<int>
    {
    }
}
