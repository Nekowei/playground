using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Nekowei.PlayGround
{
    [CreateAssetMenu(menuName = "Game/EventFloat")]
    public class FloatEventSO : GenericEventSO<float>
    {
    }
}
