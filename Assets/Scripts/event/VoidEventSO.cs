using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Nekowei.PlayGround
{
    [CreateAssetMenu(menuName = "Game/EventVoid")]
    public class VoidEventSO : ScriptableObject
    {
        public Action action;

        public void callEvent()
        {
            if (action == null)
            {
                Debug.LogError("VoidEventSO: no action found");
            }
            else
            {
                action.Invoke();
            }
        }
    }
}
